package ma.octo.assignement.domain;

import javax.persistence.*;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@Table(name = "o_deposit")
public class Deposit {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(precision = 16, scale = 2, nullable = false,name="montant_deposit")
	private BigDecimal montantDeposit;

	@Column(name="date_execution")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecution;

	@Column
	private String nom_prenom_emetteur;

	@ManyToOne
	private Compte compteBeneficiaire;

	@Column(length = 200,name="motif_deposit")
	private String motifDeposit;
	

}
