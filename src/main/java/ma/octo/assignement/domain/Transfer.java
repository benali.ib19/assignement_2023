package ma.octo.assignement.domain;

import javax.persistence.*;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.util.Date;

@Entity
@Data
@NoArgsConstructor
@Table(name = "o_transfer")
public class Transfer {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(precision = 16, scale = 2, nullable = false,name="montant_transfer")
	private BigDecimal montantTransfer;

	@Column(name = "date_execution")
	@Temporal(TemporalType.TIMESTAMP)
	private Date dateExecution;

	@ManyToOne
	private Compte compteEmetteur;

	@ManyToOne
	private Compte compteBeneficiaire;

	@Column(length = 200, name = "motif_transfer")
	private String motifTransfer;

	
}
