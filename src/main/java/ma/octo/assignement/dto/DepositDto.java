package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;

@Data
public class DepositDto {
	
	private String nomCompletEmetteur;
	private String ribBeneficiaire;
	private String motifDeposit;
	private BigDecimal montantDeposit;
	private Date date;
}
