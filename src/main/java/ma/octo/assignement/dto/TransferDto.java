package ma.octo.assignement.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Data;
@Data
public class TransferDto {
	private String nrCompteEmetteur;
	private String nrCompteBeneficiaire;
	private String motifTransfer;
	private BigDecimal montantTransfer;
	private Date date;

	
}
