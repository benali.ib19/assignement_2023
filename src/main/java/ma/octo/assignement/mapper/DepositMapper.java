package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;

public class DepositMapper {

	private static DepositDto depositDto;

    public static DepositDto map(Deposit deposit) {
    	
    	depositDto=new DepositDto();
    	depositDto.setNomCompletEmetteur(deposit.getNom_prenom_emetteur());
    	depositDto.setRibBeneficiaire(deposit.getCompteBeneficiaire().getRib());
    	depositDto.setMontantDeposit(deposit.getMontantDeposit());
    	depositDto.setMotifDeposit(deposit.getMotifDeposit());
    	depositDto.setDate(deposit.getDateExecution());

        return depositDto;

    }
}
