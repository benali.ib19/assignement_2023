package ma.octo.assignement.mapper;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;

public class TransferMapper {

	
    private static TransferDto transferDto;

    public static TransferDto map(Transfer transfer) {
    	//Ajout des autres attrinuts de dto
        transferDto = new TransferDto();
        transferDto.setNrCompteEmetteur(transfer.getCompteEmetteur().getNrCompte());
        //num compte benificiaire
        transferDto.setNrCompteBeneficiaire(transfer.getCompteBeneficiaire().getNrCompte());
        transferDto.setDate(transfer.getDateExecution());
        transferDto.setMotifTransfer(transfer.getMotifTransfer());
        //montant
        transferDto.setMontantTransfer(transfer.getMontantTransfer());

        return transferDto;

    }
}
