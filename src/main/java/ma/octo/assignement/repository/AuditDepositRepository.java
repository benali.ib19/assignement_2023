package ma.octo.assignement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ma.octo.assignement.domain.AuditDeposit;

@Repository
public interface AuditDepositRepository extends JpaRepository<AuditDeposit, Long> {

}
