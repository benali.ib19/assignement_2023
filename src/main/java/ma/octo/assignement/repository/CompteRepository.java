package ma.octo.assignement.repository;

import ma.octo.assignement.domain.Compte;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CompteRepository extends JpaRepository<Compte, Long> {
	//get compte par son N°
	Compte findByNrCompte(String nrCompte);
	//get Compte par son RIB
	Compte findByRib(String rib);
}
