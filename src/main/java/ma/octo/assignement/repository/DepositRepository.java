package ma.octo.assignement.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import ma.octo.assignement.domain.Deposit;

@Repository
public interface DepositRepository extends JpaRepository<Deposit, Long> {
	//For testing purposes
	Deposit findByMotifDeposit(String motifDeposit);
}
