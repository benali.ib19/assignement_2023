package ma.octo.assignement.service;

import ma.octo.assignement.domain.AuditDeposit;
import ma.octo.assignement.domain.AuditTransfer;
import ma.octo.assignement.domain.util.EventType;
import ma.octo.assignement.repository.AuditDepositRepository;
import ma.octo.assignement.repository.AuditTransferRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.transaction.Transactional;

@Service
@AllArgsConstructor
@NoArgsConstructor
@Transactional
public class AuditServiceImpl implements AuditService {
	
	@Autowired
    private AuditTransferRepository auditTransferRepository;
  
    @Autowired
    private AuditDepositRepository auditDepositRepository;
    
    Logger LOGGER = LoggerFactory.getLogger(AuditServiceImpl.class);
	
	@Override
	public void auditTransfer(String message) {
		LOGGER.info("Audit de l'événement {}", EventType.TRANSFER);

        AuditTransfer audit = new AuditTransfer();
        audit.setEventType(EventType.TRANSFER);
        audit.setMessage(message);
        auditTransferRepository.save(audit);
		
	}

	@Override
	public void auditDeposit(String message) {
		LOGGER.info("Audit de l'événement {}", EventType.DEPOSIT);
        //Changement de AuditTransfer ---> AuditDeposit
        //AuditTransfer audit = new AuditTransfer();
        AuditDeposit audit =new AuditDeposit();
        audit.setEventType(EventType.DEPOSIT);
        audit.setMessage(message);
        auditDepositRepository.save(audit);
	}

    

    /*
    

    
    
    */
}
