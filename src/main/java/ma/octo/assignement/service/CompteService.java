package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.domain.Compte;

public interface CompteService {

	// Get all accounts(comptes) details
	public List<Compte> loadAllCompte();
}
