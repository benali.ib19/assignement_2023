package ma.octo.assignement.service;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.repository.CompteRepository;

@Service
@Transactional
@AllArgsConstructor
@NoArgsConstructor
public class CompteServiceImpl implements CompteService {
	
	@Autowired
	private CompteRepository compteRepository;
	
	Logger LOGGER = LoggerFactory.getLogger(CompteServiceImpl.class);
	
	@Override
	public List<Compte> loadAllCompte() {
		LOGGER.info("Lister des comptes");
		List<Compte> comptes = compteRepository.findAll();

		if (CollectionUtils.isEmpty(comptes))
			return null;
		return comptes;
	}

}
