package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;

public interface DepositService {

	//Get all deposit details
	public List<Deposit> loadAllDeposits();
	
	//Make a Deposit
	public Deposit deposerArgent(DepositDto depositDto) throws CompteNonExistantException, TransactionException;
}
