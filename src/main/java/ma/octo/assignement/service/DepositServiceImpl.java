package ma.octo.assignement.service;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.DepositRepository;

@Service
@Transactional
@AllArgsConstructor
@NoArgsConstructor
public class DepositServiceImpl implements DepositService {

	public static final int MONTANT_MAXIMAL = 10000;

	@Autowired
	private DepositRepository depositRepository;
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private AuditService auditService;
	@Autowired
	private TransferServiceImpl transferServiceImpl;

	Logger LOGGER = LoggerFactory.getLogger(DepositServiceImpl.class);

	@Override
	public List<Deposit> loadAllDeposits() {
		LOGGER.info("Lister des deposits");
		List<Deposit> deposits = depositRepository.findAll();
		if (CollectionUtils.isEmpty(deposits))
			return null;

		return deposits;
	}

	@Override
	public Deposit deposerArgent(DepositDto depositDto) throws CompteNonExistantException, TransactionException {
		Compte beneficiaire = compteRepository.findByRib(depositDto.getRibBeneficiaire());
		
		//System.out.println(depositDto.getRib());
		
		// Verifier l'existence de compte du beneficiaire
		if (isAccountExist(beneficiaire)) {
			// Verifier le montant
			if (transferServiceImpl.isMontantCorrect(depositDto.getMontantDeposit())) {

				// Verifier le motif et la balance de compte
				transferServiceImpl.checkOperation(depositDto.getMotifDeposit(),depositDto.getNomCompletEmetteur(),depositDto.getDate());

				// update beneficiary account balance
				beneficiaire.setSolde(beneficiaire.getSolde().add(depositDto.getMontantDeposit()));
				compteRepository.save(beneficiaire);

				Deposit deposit = new Deposit();
				deposit.setCompteBeneficiaire(beneficiaire);
				deposit.setDateExecution(depositDto.getDate());
				deposit.setMontantDeposit(depositDto.getMontantDeposit());
				deposit.setMotifDeposit(depositDto.getMotifDeposit());
				deposit.setNom_prenom_emetteur(depositDto.getNomCompletEmetteur());

				auditService.auditDeposit("Deposit depuis " + depositDto.getNomCompletEmetteur() + " vers "
						+ depositDto.getRibBeneficiaire() + " d'un montant de "
						+ depositDto.getMontantDeposit().toString());

				return depositRepository.save(deposit);
			}

		}else {
			System.out.println(depositDto.toString());
		}
		return null;
	}

	// Verification de l'existence du compte
	public boolean isAccountExist(Compte c) throws CompteNonExistantException {
		if (c== null) {
			LOGGER.error("Compte Non Existant!");
			throw new CompteNonExistantException("Compte Non existant");
		}
		return true;
	}

}
