package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;

public interface TransferService {

	//Get all transfers details
	public List<Transfer> loadAllTransfers();
	
	//Make a transfer
	public Transfer createTransaction(TransferDto transferDto) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException;
	
}
