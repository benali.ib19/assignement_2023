package ma.octo.assignement.service;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.TransferRepository;

@Service
@AllArgsConstructor
@NoArgsConstructor
@Transactional
public class TransferServiceImpl implements TransferService {
	public static final int MONTANT_MAXIMAL = 10000;

	@Autowired
	private TransferRepository transferRepository;
	@Autowired
	private CompteRepository compteRepository;
	@Autowired
	private AuditService auditService;

	Logger LOGGER = LoggerFactory.getLogger(TransferServiceImpl.class);

	@Override
	public List<Transfer> loadAllTransfers() {
		LOGGER.info("Lister des transfers");
		List<Transfer> transfers = transferRepository.findAll();
		if (CollectionUtils.isEmpty(transfers))
			return null;

		return transfers;

	}

	@Override
	public Transfer createTransaction(TransferDto transferDto)
			throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException {
		Compte emetteur = compteRepository.findByNrCompte(transferDto.getNrCompteEmetteur());
		Compte beneficiaire = compteRepository.findByNrCompte(transferDto.getNrCompteBeneficiaire());

		//System.out.println(transferDto.getNrCompteBeneficiaire());
		
		
		// Verifier l'existence des 2 comptes
		if (isAccountExist(emetteur) && isAccountExist(beneficiaire)) {
			// Verifier le montant
			if (isMontantCorrect(transferDto.getMontantTransfer())) {
				// Verifier le motif et la balance de compte
				checkOperation(transferDto.getMotifTransfer(), emetteur.getSolde(), transferDto.getMontantTransfer(),transferDto.getDate());
				// update source account balance
				emetteur.setSolde(emetteur.getSolde().subtract(transferDto.getMontantTransfer()));
				compteRepository.save(emetteur);
				// update beneficiary account balance
				beneficiaire.setSolde(beneficiaire.getSolde().add(transferDto.getMontantTransfer()));
				compteRepository.save(beneficiaire);

				Transfer transfer = new Transfer();
				transfer.setDateExecution(transferDto.getDate());
				transfer.setCompteBeneficiaire(beneficiaire);
				transfer.setCompteEmetteur(emetteur);
				transfer.setMontantTransfer(transferDto.getMontantTransfer());
				transfer.setMotifTransfer(transferDto.getMotifTransfer());
				// Enregistrement du transfer

				auditService.auditTransfer("Transfer depuis " + transferDto.getNrCompteEmetteur() + " vers "
						+ transferDto.getNrCompteBeneficiaire() + " d'un montant de "
						+ transferDto.getMontantTransfer().toString());

				return transferRepository.save(transfer);
			}

		}
		
		return null;

	}

	// Verification de l'existence du compte
	public boolean isAccountExist(Compte c) throws CompteNonExistantException {
		if (c == null) {
			LOGGER.error("Compte Non Existant!");
			throw new CompteNonExistantException("Compte Non existant");
		}
		return true;
	}

	// Verification du montant de transfer
	public boolean isMontantCorrect(BigDecimal m) throws TransactionException {
		if (m.equals(null) || m.compareTo(BigDecimal.valueOf(0)) == 0) {
			LOGGER.error("Montant vide");
			throw new TransactionException("Montant vide");
		} else if (m.compareTo(BigDecimal.valueOf(10)) < 0) {
			LOGGER.error("Montant minimal de transfer non atteint");
			throw new TransactionException("Montant minimal de transfer non atteint");
		} else if (m.compareTo(BigDecimal.valueOf(MONTANT_MAXIMAL)) > 0) {
			System.out.println("Montant maximal de transfer dépassé");
			throw new TransactionException("Montant maximal de transfer dépassé");
		}
		return true;
	}

	// Verifier le motif et la balance de compte
	public void checkOperation(String s, BigDecimal A, BigDecimal B,Date d)
			throws TransactionException, SoldeDisponibleInsuffisantException {
		if (s.isEmpty()) {
			LOGGER.error("Motif vide!");
			throw new TransactionException("Motif vide");
		}

		if (A.compareTo(B) < 0) {
			LOGGER.error("Solde insuffisant pour l'utilisateur");
			throw new SoldeDisponibleInsuffisantException("Solde insuffisant pour l'utilisateur");
		}
		
		if(d==null) {
			LOGGER.error("Date vide!");
			throw new TransactionException("Date vide");
		}
	}

	// Verifier le motif
	public void checkOperation(String s1, String s2,Date d)
			throws TransactionException {
		if (s1.isEmpty()) {
			LOGGER.error("Motif vide!");
			throw new TransactionException("Motif vide");
		}
		if(s2.isEmpty()) {
			LOGGER.error("Nom complet de l'emetteur est vide!");
			throw new TransactionException("Nom complet de l'emetteur est vide");
		}
		if(d==null) {
			LOGGER.error("Date vide!");
			throw new TransactionException("Date est vide");
		}

	}

}