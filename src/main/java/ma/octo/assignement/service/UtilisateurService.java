package ma.octo.assignement.service;

import java.util.List;

import ma.octo.assignement.domain.Utilisateur;

public interface UtilisateurService {

	// Get all users(utilisateurs) details
	public List<Utilisateur> loadAllUtilisateur();
}
