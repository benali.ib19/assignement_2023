package ma.octo.assignement.service;

import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.repository.UtilisateurRepository;

@Service
@Transactional
@AllArgsConstructor
@NoArgsConstructor
public class UtilisateurServiceImpl implements UtilisateurService {
	
	@Autowired
	private UtilisateurRepository utilisateurRepository;
	
	Logger LOGGER = LoggerFactory.getLogger(TransferServiceImpl.class);
	
	@Override
	public List<Utilisateur> loadAllUtilisateur() {
		LOGGER.info("Lister des utilisateurs");
		List<Utilisateur> users = utilisateurRepository.findAll();

        if (CollectionUtils.isEmpty(users)) 
            return null;
        
        return users;
	}

}
