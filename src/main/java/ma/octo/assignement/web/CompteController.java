package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.service.CompteService;

@RestController
@RequestMapping("/accounts")
@AllArgsConstructor
public class CompteController {

	@Autowired
	private CompteService compteService;
	
	//Get accounts
    @GetMapping()
    public ResponseEntity<List<Compte>> loadAllCompte(){
		List<Compte> comptes=compteService.loadAllCompte();
		return new ResponseEntity<>(comptes,HttpStatus.OK);
	}
}
