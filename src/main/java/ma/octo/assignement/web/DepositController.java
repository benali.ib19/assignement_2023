package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.Deposit;
import ma.octo.assignement.dto.DepositDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.DepositService;

@RestController
@RequestMapping("/deposits")
@AllArgsConstructor
public class DepositController {

	@Autowired
	private DepositService depositService;

	// Get deposits
	@GetMapping()
	public ResponseEntity<List<Deposit>> loadAll() {
		List<Deposit> deposits = depositService.loadAllDeposits();
		return new ResponseEntity<>(deposits, HttpStatus.OK);
	}

	// Make a deposit
	@PostMapping("/makeDeposit")
	public ResponseEntity<Deposit> deposerArgent(@RequestBody DepositDto depositDto)
			throws CompteNonExistantException, TransactionException{
		Deposit deposit = depositService.deposerArgent(depositDto);
		return new ResponseEntity<>(deposit, HttpStatus.CREATED);
	}
}
