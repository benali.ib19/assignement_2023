package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.Transfer;
import ma.octo.assignement.dto.TransferDto;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.service.TransferService;

@AllArgsConstructor
@RestController
@RequestMapping("/transfers")
class TransferController {

	@Autowired
	private TransferService transferService;
    
	//Get transfers
	@GetMapping()
	public ResponseEntity<List<Transfer>> loadAllTransfers(){
		List<Transfer> transfers=transferService.loadAllTransfers();
		return new ResponseEntity<>(transfers,HttpStatus.OK);
	}

    
    //Make a transfer
    @PostMapping("/makeTransfer")
	public ResponseEntity<Transfer> createTransaction(@RequestBody TransferDto transferDto) throws CompteNonExistantException, TransactionException, SoldeDisponibleInsuffisantException{
		Transfer transfer=transferService.createTransaction(transferDto);
		return new ResponseEntity<>(transfer,HttpStatus.CREATED);
	}
    
}
