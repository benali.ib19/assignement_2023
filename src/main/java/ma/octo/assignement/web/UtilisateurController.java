package ma.octo.assignement.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import lombok.AllArgsConstructor;
import ma.octo.assignement.domain.Utilisateur;
import ma.octo.assignement.service.UtilisateurService;

@RestController
@RequestMapping("/users")
@AllArgsConstructor
public class UtilisateurController {
	
	@Autowired
	private UtilisateurService utilisateurService;
	
	//Get users
    @GetMapping()
    public ResponseEntity<List<Utilisateur>> loadAllUtilisateur(){
		List<Utilisateur> utilisateurs=utilisateurService.loadAllUtilisateur();
		return new ResponseEntity<>(utilisateurs,HttpStatus.OK);
	}
    

}
