package ma.octo.assignement.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Deposit;

@DataJpaTest
public class DepositRepositoryTest {

	@Autowired
	private DepositRepository underTest;

	@AfterEach
	void delete() {
		underTest.deleteAll();
	}

	@Test
	public void itShouldfindOne() {
		// given
		Deposit deposit = new Deposit();
		deposit.setCompteBeneficiaire(null);
		deposit.setDateExecution(new Date());
		deposit.setMontantDeposit(BigDecimal.valueOf(8000));
		deposit.setMotifDeposit("Deposit test");
		deposit.setNom_prenom_emetteur("Ben-Ali Imad");

		underTest.save(deposit);
		// when
		Deposit expected = underTest.findByMotifDeposit("Deposit test");
		// then
		assertThat(expected).isEqualTo(deposit);

	}

	@Test
	public void itShouldfindAll() {

		// given
		Deposit deposit = new Deposit();
		deposit.setCompteBeneficiaire(null);
		deposit.setDateExecution(new Date());
		deposit.setMontantDeposit(BigDecimal.valueOf(8000));
		deposit.setMotifDeposit("Deposit test");
		deposit.setNom_prenom_emetteur("Ben-Ali Imad");

		underTest.save(deposit);
		// when
		List<Deposit> deposits = underTest.findAll();
		boolean full = !CollectionUtils.isEmpty(deposits);
		// then
		assertThat(full).isTrue();

	}
}
