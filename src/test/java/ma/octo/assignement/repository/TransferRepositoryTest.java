package ma.octo.assignement.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.util.CollectionUtils;

import ma.octo.assignement.domain.Transfer;

@DataJpaTest
public class TransferRepositoryTest {

	@Autowired
	private TransferRepository underTest;

	@AfterEach
	void delete() {
		underTest.deleteAll();
	}

	@Test
	public void itShouldfindOne() {
		// given
		Transfer transfer = new Transfer();
		transfer.setMontantTransfer(BigDecimal.TEN);
		transfer.setCompteBeneficiaire(null);
		transfer.setCompteEmetteur(null);
		transfer.setDateExecution(new Date());
		transfer.setMotifTransfer("Assignment test 2021");

		underTest.save(transfer);
		// when
		Transfer expected = underTest.findByMotifTransfer("Assignment test 2021");
		// then
		assertThat(expected).isEqualTo(transfer);

	}

	@Test
	public void itShouldfindAll() {

		// given
		Transfer transfer = new Transfer();
		transfer.setMontantTransfer(BigDecimal.TEN);
		transfer.setCompteBeneficiaire(null);
		transfer.setCompteEmetteur(null);
		transfer.setDateExecution(new Date());
		transfer.setMotifTransfer("Assignment test 2021");

		underTest.save(transfer);
		// when
		List<Transfer> transfers = underTest.findAll();
		boolean full = !CollectionUtils.isEmpty(transfers);
		// then
		assertThat(full).isTrue();

	}
}